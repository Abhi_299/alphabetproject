﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using UnityEngine.SceneManagement;

public class LocalizationManager : MonoBehaviour
{

    public static LocalizationManager instance;
    public AudioSource voiceOverPlayer;
    //public string usFileName, ukFileName;
    public DiffrentLanguagesAudioManager languageAudioManager;
    internal static string languageFileName;

    //private Dictionary<string, string> localizedText;
    //private bool isReady = false;
    private string resultTextString = "Localized text not found";
    private AudioClip resultAudioClip;


    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public void LoadLocalizedText(string fileName)
    {
        languageFileName = fileName;
        SceneManager.LoadScene(0);
    }
    //public void LoadLocalizedText(string fileName)
    //{
        //localizedText = new Dictionary<string, string>();

        //string filePath = Path.Combine(Application.dataPath + "/", fileName + ".xml");

        //#if !UNITY_EDITOR
        //        filePath = Path.Combine("jar:file://" + Application.dataPath + "!/assets/", fileName + ".xml");
        //#endif

        //Debug.Log("PathFile- "+filePath);


        //if (File.Exists(filePath))
        //{
        //    string dataAsJson = File.ReadAllText(filePath);

        //    LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

        //    for (int i = 0; i < loadedData.items.Count; i++)
        //    {
        //        localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
        //    }

        //    Debug.Log("Data loaded, dictionary contains: " + localizedText.Count + " entries");
        //}
        //else
        //{
        //    Debug.LogError("Cannot find file!");
        //}

        //languageFileName = fileName;
        //isReady = true;
    //}

    //public string GetLocalizedValue(string key)
    //{
    //    string result = resultTextString;
    //    if (localizedText.ContainsKey(key))
    //    {
    //        result = localizedText[key];
    //    }
    //    return result;

    //}

    //public bool GetIsReady()
    //{
    //    return isReady;
    //}

    public void SetAudioAndText(string clipName) {

		Debug.Log ("Clip to set--"+clipName);

        if (languageFileName == "US")
        {
            for (int i = 0; i < languageAudioManager.audioFiles.Count; i++)
            {
				if (languageAudioManager.audioFiles[i].key.ToLower() == clipName.ToLower())
                {
                    resultAudioClip = languageAudioManager.audioFiles[i].us_Audio;
                    resultTextString = languageAudioManager.audioFiles[i].us_Text;
                    break;
                }
                else {
                    resultAudioClip = null;
                    resultTextString = "Localized text not found";
                }
            }
           
        }
        else {

            for (int i = 0; i < languageAudioManager.audioFiles.Count; i++)
            {

				if (languageAudioManager.audioFiles[i].key.ToLower() == clipName.ToLower())
                {
                    resultAudioClip = languageAudioManager.audioFiles[i].uk_Audio;
                    resultTextString = languageAudioManager.audioFiles[i].uk_Text;

					Debug.Log ("ClipName--"+clipName);
                    break;
                }
                else
                {
                    resultAudioClip = null;
                    resultTextString = "Localized text not found";
                }
            }
        }

//        Debug.Log("Selected Language- " + languageFileName);
    }

    public void PlayAudioClip() {
        if (resultAudioClip != null)
        {
            voiceOverPlayer.PlayOneShot(resultAudioClip);
        }
        else {
            Debug.LogError("Voice over not Found");
        }
    }

    public string GetLocalizedValue()
    {
        return resultTextString;
    }

}