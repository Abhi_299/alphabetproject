﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    public bool playVoiceOver;
    //public string voiceOverType;
    public string key="";

    private Text _text;

    AssetBundle _assetBundle;

    void Awake() {
        _text = GetComponent<Text>();
    }

    void OnEnable() {

        LocalizationManager.instance.SetAudioAndText(key);

        if (playVoiceOver) {
            LocalizationManager.instance.PlayAudioClip();
        }
        LoadText();
    }

    public void LoadText() {
        _text.text = LocalizationManager.instance.GetLocalizedValue();
    }

}