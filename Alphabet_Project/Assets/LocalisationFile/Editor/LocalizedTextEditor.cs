﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Xml;

public class LocalizedTextEditor : EditorWindow
{
    public LocalizationData localizationData;
    LocalizationItem _localizationItem;             //serialized class of LocalizationData
    AudioFile _languageFile;                        //serialized class of scriptable object (DiffrentLanguagesAudioManager)

    Vector2 scrollPos;
    string fileName = "File Name- ";               //To show loaded language name in localized text window
    int itemsIndex=1;
    //static int removingIndex;

    [MenuItem("Window/Localized Text Editor")]
    static void Init()
    {
        EditorWindow.GetWindow(typeof(LocalizedTextEditor)).Show();
    }

    private void OnGUI()
    {
        GUILayout.Label(fileName);
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

        if (GUILayout.Button("Copy data"))
        {
            Copy_Data();
        }

        if (localizationData != null)
        {
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty serializedProperty = serializedObject.FindProperty("localizationData");
            EditorGUILayout.PropertyField(serializedProperty, true);
            serializedObject.ApplyModifiedProperties();

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("+", GUILayout.Width(20), GUILayout.Height(20)))
            {
                localizationData.items.Add(_localizationItem);
            }

            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Save data"))
            {
                SaveGameData();
            }

        }

        if (GUILayout.Button("Load data"))
        {
            LoadGameData();
        }

        if (GUILayout.Button("Create new data"))
        {
            CreateNewData();
        }

        EditorGUILayout.EndScrollView();
    }

    //To save data in to xml file
    private void SaveGameData()
    {
        string filePath = EditorUtility.SaveFilePanel("Save localization data file", Application.streamingAssetsPath, "", "xml");

        if (!string.IsNullOrEmpty(filePath))
        {
            string dataAsJson = JsonUtility.ToJson(localizationData);
            File.WriteAllText(filePath, dataAsJson);
        }
    }

    private void CreateNewData()
    {
        localizationData = new LocalizationData();
        fileName = "File Name- ";
    }

    //To Load xml file
    void LoadGameData() {
        string filePath = EditorUtility.OpenFilePanel("Select localization data file", Application.streamingAssetsPath, "xml");

        if (!string.IsNullOrEmpty(filePath))
        {
            string dataAsXml = File.ReadAllText(filePath);

            fileName ="File Name- "+ Path.GetFileName(filePath);

            localizationData = JsonUtility.FromJson<LocalizationData>(dataAsXml);
        }
    }

    //To copy data from xml file to scripatable object.
    void Copy_Data() {

        if ((!localizationData.UkEnglish && !localizationData.UsEnglish)|| (localizationData.UkEnglish && localizationData.UsEnglish)) {
            Debug.LogError("Select one language");
            return;
        }

        for (int i = 0; i < localizationData.items.Count; i++)
        {
            if (localizationData.languageManagerScriptableObject.audioFiles.Count <= i)
            {
                AudioFile _languageFile=new AudioFile();
                localizationData.languageManagerScriptableObject.audioFiles.Add(_languageFile);
            }

            localizationData.languageManagerScriptableObject.audioFiles[i].key = localizationData.items[i].key;

            if (localizationData.UkEnglish)
            {
                localizationData.languageManagerScriptableObject.audioFiles[i].uk_Text = localizationData.items[i].value;
            }
            else if (localizationData.UsEnglish)
            {
                localizationData.languageManagerScriptableObject.audioFiles[i].us_Text = localizationData.items[i].value;
            }
        }
    }

}