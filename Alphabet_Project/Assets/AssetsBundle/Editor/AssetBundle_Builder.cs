﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AssetBundle_Builder : MonoBehaviour {

    [MenuItem("Assets/Build_Asset Bundle")]
    static void Build_AssetBundle() {
    #if UNITY_ANDROID
        BuildPipeline.BuildAssetBundles("AssetsBundle/Android", BuildAssetBundleOptions.None, BuildTarget.Android);
#endif

#if UNITY_IPHONE
		BuildPipeline.BuildAssetBundles ("AssetsBundle/IOS", BuildAssetBundleOptions.None, BuildTarget.iOS);
#endif

#if UNITY_STANDALONE
		BuildPipeline.BuildAssetBundles ("AssetsBundle", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows );
#endif
    }
}
