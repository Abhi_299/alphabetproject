﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LanguageAudioManager", menuName = "WizAr/Create Language Audio Manager", order = 0)]
public class DiffrentLanguagesAudioManager : ScriptableObject {
    public List<AudioFile> audioFiles;
}

[System.Serializable]
public class AudioFile : System.Object {

    public string key;
    public AudioClip us_Audio,uk_Audio;
    public string us_Text, uk_Text;
}
