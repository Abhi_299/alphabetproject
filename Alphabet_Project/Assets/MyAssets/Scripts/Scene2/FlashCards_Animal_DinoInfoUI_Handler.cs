﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;

public class FlashCards_Animal_DinoInfoUI_Handler : MonoBehaviour {

	#region public variables 
	// default scale value of dino in 3d modules.
	  public float defaultScaleValue;
	  public Text headlinetext, descriptionText;
	  public Material skyboxLand, skyboxUnderWater, skyboxSnow, skyboxCave, skyboxTransport;
	  public GameObject listBtn, backBtn, instructionPanel, scrollView_Panel, arCamera;
	  public GameObject mAnimalBtnPrfabs;
	  public GameObject dinoBtn_Parent, ThreeDView;
      public LoadAlphabet_OnDetection mLoadingModels;
      //public FlashCard_AnimalScriptableObject[] mAnimalScriptableObject;
      public List<string> modelNames = new List<string>();
    public GameObject mDownloadScreen;
    GameObject currentGameObject = null;
	  public static FlashCards_Animal_DinoInfoUI_Handler instace;
    Coroutine loadmodelcoroutine = null;
    public bool mIsTestPhase = false;
	#endregion

	#region private variables
	private bool isModelDisplay = false;
	private int bgCount=0;
    private int mCenterValue = 0;
    Coroutine mLoadingModelRoutine = null ;
    bool mCheck = false;
    int mPrevValue = -1;
    #endregion

    void OnEnable()
	{
		isModelDisplay = false;
        CustomScrollView.centerItemInfo += HandleCenterInfo;
        LoadAlphabet_OnDetection.isModelLoaded += HandleLoadModel;
		FlashCards_Animal_CanvasSwitch.threeD_ModulesActive += HandleThreeD_ModulesActive;
        LoadAlphabet_OnDetection.isModelLoaded += DisplayModel;

        Debug.Log("Is model Loaded OnEnable");
	}

	void OnDisable()
	{
        CustomScrollView.centerItemInfo -= HandleCenterInfo;
        LoadAlphabet_OnDetection.isModelLoaded -= HandleLoadModel;
        FlashCards_Animal_CanvasSwitch.threeD_ModulesActive -= HandleThreeD_ModulesActive;
        LoadAlphabet_OnDetection.isModelLoaded -= DisplayModel;
    }


	void Start() {
		instace = this;

	}

    public void Handle3DLayout(bool p3D)
    {
        mCheck = false;
        for (int j = 0; j < dinoBtn_Parent.transform.childCount; j++)
        {
            Destroy(dinoBtn_Parent.transform.GetChild(j).gameObject);
        }

        if (p3D)
        {
            if (mDownloadScreen != null)
            {
                mDownloadScreen.SetActive(true);
            }
            StartCoroutine(LoadModelsInloop());
        }
        
    }

    IEnumerator LoadModelsInloop()
    {
        Debug.Log("Total model- "+modelNames.Count);
        for (int i = 0; i < modelNames.Count; i++)
        {
            if (mIsTestPhase)
            {
//				Debug.Log ("Model loaded 3D");
                yield return StartCoroutine(LoadAlphabet_OnDetection.instance.OnTrackingFound(modelNames[i], ThreeDView.transform, true));
            }
            //else
            //{
                //yield return StartCoroutine(LoadAlphabet_OnDetection.instance.LoadModel(modelNames[i], ThreeDView.transform));
            //}

        }

        if(mDownloadScreen != null)
        {
            mDownloadScreen.SetActive(false);
            mCheck = true;
           mCenterValue = 0;
           ActivateModel();
        }


    }

    #region delegate 

	private void HandleLoadModel(string objectName, Transform _transform, bool status, LoadAlphabet_OnDetection.SceneType sceneType)
    {
		Debug.Log("model loaded or not " + _transform.name);

        if (status && sceneType==LoadAlphabet_OnDetection.SceneType.ThreeD && !isModelDisplay )
        {
            currentGameObject = _transform.gameObject;
			Debug.Log(currentGameObject.name);
			Debug.Log(currentGameObject.GetComponent<Alphabet_ModelController>()==null);
			var aAnimalScriptableObject = currentGameObject.GetComponent<Alphabet_ModelController>().modelInfoObject;
            GameObject dinoInstance = Instantiate(mAnimalBtnPrfabs, dinoBtn_Parent.transform, false);
            var aImage = dinoInstance.transform.Find("AnimalImage").GetComponentInChildren<Image>();
            aImage.sprite = aAnimalScriptableObject._sprite;
            aImage.preserveAspect = true;
			dinoInstance.name = objectName;
            dinoInstance.transform.Find("AnimalName_Text").GetComponent<Text>().text = aAnimalScriptableObject.modelName;
            dinoInstance.transform.Find("AnimalImage").name = aAnimalScriptableObject.modelName;
            var aButton = dinoInstance.transform.Find("Animal_Button").GetComponent<Button>();

            aButton.onClick.AddListener(delegate { CloseScrollViewPanel(false); });
            
        }
        else
        {
            currentGameObject = null;
        }

        if (_transform.GetComponent<F_Alpha_ActivityTransformHandler>() != null)
        {
            _transform.localEulerAngles = _transform.GetComponent<F_Alpha_ActivityTransformHandler>().rotation_3D;
            //_transform.localScale = new Vector3(_transform.GetComponent<F_Alpha_ActivityTransformHandler>().size_3D, _transform.GetComponent<F_Alpha_ActivityTransformHandler>().size_3D, _transform.GetComponent<F_Alpha_ActivityTransformHandler>().size_3D);
            ThreeDView.transform.localEulerAngles = Vector3.zero;
            //Debug.Log("3D model scale- " + _transform.GetComponent<F_Alpha_ActivityTransformHandler>().size_3D);
        }

    }

    void HandleCenterInfo(int centerValue)
	{

        if (centerValue != mPrevValue)
        {
           
            mCenterValue = centerValue;
            mPrevValue = mCenterValue;

            if (mCheck)
            {
               
                ActivateModel();
            }
        }

	}

	void HandleThreeD_ModulesActive(bool status)
	{
        arCamera.transform.localRotation = Quaternion.Euler(0,0,0);
		CloseScrollViewPanel(status);
	}
	#endregion

	#region public methods 

    public void ActivateModel()
    {
        Active3DModel(mCenterValue);
    }

	public void Active3DModel(int pCenterValue)
    {
		for (int i=0; i< ThreeDView.transform.childCount;i++)
		{
			Destroy(ThreeDView.transform.GetChild(i).gameObject);
		}

        

        if (pCenterValue < modelNames.Count)
        {
            var aAnimalName = modelNames[pCenterValue];

            StartLoadingModel(aAnimalName);
        }
	
	}

    public void StartLoadingModel(string pAnimalName)
    {
        if(mLoadingModelRoutine != null)
        {
            StopCoroutine(mLoadingModelRoutine);
            mLoadingModelRoutine = null;
        }

        
		isModelDisplay = true;

        if(mIsTestPhase)
         mLoadingModelRoutine = StartCoroutine(LoadAlphabet_OnDetection.instance.OnTrackingFound(pAnimalName , ThreeDView.transform,true));
		Debug.Log ("Model loaded 3D");
        //else
            //mLoadingModelRoutine = StartCoroutine(LoadAlphabet_OnDetection.instance.LoadModel(pAnimalName, ThreeDView.transform));


    }


    public void  DisplayModel(string pName, Transform pTransform, bool pStatus, LoadAlphabet_OnDetection.SceneType sceneType)
    {
		if (pStatus && sceneType==LoadAlphabet_OnDetection.SceneType.ThreeD && isModelDisplay)
        {
            isModelDisplay = false;
			var aModel = pTransform.gameObject;
            var aAnimalModelScript = aModel.GetComponent<Alphabet_ModelController>().modelInfoObject;
//            Renderer[] rendererComponents = aModel.GetComponentsInChildren<Renderer>(true);
//            Collider[] colliderComponents = aModel.GetComponentsInChildren<Collider>(true);

            // Enable rendering:
//            foreach (Renderer component in rendererComponents)
//            {
//                component.enabled = true;
//            }
//
//            foreach (Collider component in colliderComponents)
//            {
//                component.enabled = true;
//            }
            if (aModel != null)
            {
//				GameObject modelInstance = Instantiate(aModel,ThreeDView.transform, true) as GameObject;
//				modelInstance.name = pName;
//                modelInstance.transform.localRotation = aAnimalModelScript._rotation;

//                var aSkins = modelInstance.GetComponentsInChildren<SkinnedMeshRenderer>();
//
//                for (int m = 0; m < aSkins.Length; m++)
//                {
//                    aSkins[m].enabled = true;
//                }
//
//                if (modelInstance.transform.localScale.x < defaultScaleValue)
//                {
//                    modelInstance.transform.localScale = new Vector3(defaultScaleValue, defaultScaleValue, defaultScaleValue);
//                }

                if (aAnimalModelScript.isWaterAnimal && skyboxUnderWater != null)
                {
                    RenderSettings.skybox = skyboxUnderWater;
                }
                else if (aAnimalModelScript.isSnowAnimal && skyboxSnow != null)
                {
                    RenderSettings.skybox = skyboxSnow;
                }
                else if (aAnimalModelScript.isCaveAnimal && skyboxCave != null)
                {
                    RenderSettings.skybox = skyboxCave;
                }
				else if (aAnimalModelScript.isTransport && skyboxTransport != null)
                {
					RenderSettings.skybox = skyboxTransport;
				}
				else if (!aAnimalModelScript.isWaterAnimal && skyboxLand != null)
				{
					RenderSettings.skybox = skyboxLand;
				}

                DinoInformation(aModel.gameObject);

            }
        }
    }

   

   

	public void CloseScrollViewPanel(bool status)
	{
		scrollView_Panel.SetActive(status);
		listBtn.SetActive(!status);
		backBtn.SetActive(!status);
		instructionPanel.SetActive(!status);
		if (!status) {
			Alphabet_SoundManager.instance.PlayIdleSound ();
		} else {
			Alphabet_SoundManager.instance.StopIdleSound ();
		}
	}

	#endregion

	#region private variables.

	void DinoInformation(GameObject pCurrentObject)
	{
		if (headlinetext!=null && descriptionText != null)
		{
            var aAnimalScriptableObject = pCurrentObject.GetComponent<Alphabet_ModelController>().modelInfoObject;
			headlinetext.text = aAnimalScriptableObject.modelName;
			descriptionText.text = aAnimalScriptableObject.modelDescription;
		}
	}

	#endregion

}
