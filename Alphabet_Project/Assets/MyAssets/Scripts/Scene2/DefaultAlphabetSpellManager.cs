﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DefaultAlphabetSounds", menuName = "WizAr/Create Alphabets Sound Manager", order = 0)]
public class DefaultAlphabetSpellManager : ScriptableObject {
    public List<Aplhabet> letters;
}

[System.Serializable]
public class Aplhabet : System.Object
{
    public string letter;
    public AudioClip spell;
}