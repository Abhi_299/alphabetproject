﻿using UnityEngine;

[CreateAssetMenu(fileName ="ModelScriptableObject", menuName = "WizAr/Create Model ScriptableObject", order = 2)]
public class Alphabet_ModelScriptableObject : ScriptableObject {

    public string modelName;
    public string alphabetCharacter;
    public AudioClip idleSound;
    public string modelDescription;
    public Sprite _sprite;
    public bool isWaterAnimal,isCaveAnimal,isSnowAnimal,isTransport;
    public Quaternion _rotation;
}
