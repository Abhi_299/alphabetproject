﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Alphabet_PlayRecordDeleteHandler : MonoBehaviour {

    public static Alphabet_PlayRecordDeleteHandler instance;
    public GameObject eachCharUi;
    public Transform charSpellParent;
    public bool isMarkerDetected;
    public DefaultAlphabetSpellManager alphaSpellManager;
    public DiffrentLanguagesAudioManager _languageManager;

    private char[] _spelling;
    private GameObject character;
    private AudioClip _currAlphabetAudio;
    private GameObject currItem;
    private bool isPlayBtnClick;
    private string currModelName;

    // Use this for initialization
    void Awake() {
        instance = this;
    }

//	void OnEnable() {
//		LoadAlphabet_OnDetection.isModelLoaded += Model_Loading;
//	}
//
//	void OnDisable() {
//		LoadAlphabet_OnDetection.isModelLoaded -= Model_Loading;
//	}
//
//	public void Model_Loading(string name, Transform _transform, bool status, LoadAlphabet_OnDetection.SceneType scenetype) {
//		if (!status) {
//			StopAllCoroutines ();
//		}
//	}

    public void SetSpelling(Alphabet_ModelController _alphabetModelController) {

        currModelName = _alphabetModelController.modelInfoObject.modelName;
        _spelling = currModelName.ToCharArray();
        currItem = _alphabetModelController.gameObject;

        //if (_alphabetModelController.modelInfoObject.alphabetAudio != null)
        //{
        //    _currAlphabetAudio = _alphabetModelController.modelInfoObject.alphabetAudio;
        //}
        LocalizationManager.instance.SetAudioAndText(currModelName);

    }

    public void PlaySpelling() {
        Alphabet_SoundManager.instance.CancelInvoke("AfterPlayingSound");
        Alphabet_SoundManager.instance.isSoundPlaying = true;
        Alphabet_SoundManager.instance.StopIdleSound();
        isPlayBtnClick = true;
        Alphabet_UIController.instance.SetBtnIntraction(false);
        StopCoroutine("TriggerSpelling");
        if (GetIsRecordingAvailable(currItem.transform))
        {
            PlayRecording();
        }
        StartCoroutine(TriggerSpelling());
    }

    public void RecordingHandler() {
        isPlayBtnClick = false;
        StopCoroutine("TriggerSpelling");
        GetComponent<RecordController>().record(currItem.name.ToString(), _spelling.Length + 1);
        StartCoroutine(TriggerSpelling());
    }

    public IEnumerator TriggerSpelling() {
        for (int i = 0; i < _spelling.Length; i++)
        {
            if (isMarkerDetected)
            {
                character = Instantiate(eachCharUi,charSpellParent);
                character.transform.GetComponentInChildren<Text>().text = _spelling[i].ToString().ToUpper();
                character.name = _spelling[i].ToString().ToUpper();

                if (!GetIsRecordingAvailable(currItem.transform) && isPlayBtnClick)
                {
                    character.GetComponent<AudioSource>().clip = alphaSpellManager.letters.Find(item => item.letter.ToLower() == _spelling[i].ToString().ToLower()).spell;
                    character.GetComponent<AudioSource>().Play();
                }

                yield return new WaitForSeconds(1);
            }
            else {
                yield break;
            }
        }

        if (!GetIsRecordingAvailable(currItem.transform) && isMarkerDetected && isPlayBtnClick)
        {
            //if (_currAlphabetAudio != null)
            //{
            //    character.GetComponent<AudioSource>().clip = _currAlphabetAudio;
            //    character.GetComponent<AudioSource>().Play();
            //}
            LocalizationManager.instance.PlayAudioClip();
        }
      

        CancelInvoke("ResetSpelling");

        if (character != null && character.GetComponent<AudioSource>().clip != null)
            Invoke("ResetSpelling", (character.GetComponent<AudioSource>().clip.length + 0.5f));
        else
            Invoke("ResetSpelling", 2f);

    }

    public void ResetSpelling() {
		StopAllCoroutines ();
        Alphabet_SoundManager.instance.isSoundPlaying = false;
        if (charSpellParent != null && charSpellParent.childCount > 0) {
            for (int i = 0; i < charSpellParent.childCount; i++)
            {
                Destroy(charSpellParent.GetChild(i).gameObject);
            }
        }
        Alphabet_UIController.instance.SetBtnIntraction(true);
        if (Alphabet_UIController.instance.recordingMsgPanel != null)
        {
            Alphabet_UIController.instance.recordingMsgPanel.SetActive(false);
            Alphabet_UIController.instance.SetBtnIntraction(true);
        }
        Alphabet_SoundManager.instance.PlayIdleSound();
    }

    public void DeleteRecording() {
        transform.GetComponent<RecordController>().delete(currItem.name.ToString());
    }

    // play the clip if found
    private void PlayRecording()
    {
        GetComponent<RecordController>().play(currItem.name.ToString() + "_1");
    }

    public bool CheckRecordingAvalable()
    {
		if (currItem != null) {
			if (GetIsRecordingAvailable(currItem.transform))
				return true;
			else
				return false;
		}
		return false;
    }

    // checks whether the custom clip is available or not
    private bool GetIsRecordingAvailable(Transform currentItem)
    {
        if (currentItem != null && GetComponent<RecordController>().isFileAvailable(currentItem.name))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
