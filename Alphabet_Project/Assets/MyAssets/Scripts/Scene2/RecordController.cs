﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class RecordController : MonoBehaviour {

	//A boolean that flags whether there's a connected microphone  
	private bool micConnected = false;  

	//The maximum and minimum available recording frequencies  
	private int minFreq;  
	private int maxFreq;  
	private Hashtable metadata = new Hashtable ();
	//A handle to the attached AudioSource  
	private AudioSource goAudioSource;  
	private AudioClip clip;

	//public static string myPath = "/Profiles/Audio/";
	public static string myPath = "/Audio/";
	public delegate void recordingCompleteAndSavedAction();
	public static event recordingCompleteAndSavedAction onRecordingSaved;

	public delegate void recordingDeletedAction();
	public static event recordingDeletedAction onRecordingDeleted;

	// Use this for initialization
	void Start()   
	{  

		//Check if there is at least one microphone connected  
		if(Microphone.devices.Length <= 0)  
		{  
			//Throw a warning message at the console if there isn't  
			Debug.LogWarning("Microphone not connected!");  
		}  
		else //At least one microphone is present  
		{  
			//Set 'micConnected' to true  
			micConnected = true;  

			//Get the default microphone recording capabilities  
			Microphone.GetDeviceCaps(null, out minFreq, out maxFreq);  

			//According to the documentation, if minFreq and maxFreq are zero, the microphone supports any frequency...  
			if(minFreq == 0 && maxFreq == 0)  
			{  
				//...meaning 44100 Hz can be used as the recording sampling rate  
				maxFreq = 44100;  
			}  

			//Get the attached AudioSource component  
			goAudioSource = this.GetComponent<AudioSource>();  
		}  
	}  

	public void record(string fileName,int seconds) {		
		//If there is a microphone  
		if(micConnected)  
		{  
			//If the audio from any microphone isn't being captured  
			if (!Microphone.IsRecording (null)) {  
                goAudioSource.clip = Microphone.Start (null, true, seconds, maxFreq);   
				metadata.Clear ();
				metadata.Add ("fileName", fileName);
				metadata.Add ("delay", seconds);
				StartCoroutine ("_save", metadata);
            }
            else {
                Alphabet_UIController.isRecordedFileAvailable = false;
            }
        }  
		else // No microphone  
		{  
			Debug.Log ("Microphone not connected!");
            Alphabet_UIController.isRecordedFileAvailable = false;
        }
    }

	public void stopRecording () {
		StopCoroutine ("_save");
		Microphone.End (null);
	}

	IEnumerator _save (Hashtable metaData) {
		yield return new WaitForSeconds (float.Parse(metaData["delay"].ToString()));
		save ();
		if(onRecordingSaved!=null) {
			onRecordingSaved ();
			Microphone.End (null);
			RefreshEditorProjectWindow();
            Alphabet_UIController.isRecordedFileAvailable = true;

        }
    }
		

	private void save() {
		SavWav.Save (metadata["fileName"].ToString(), goAudioSource.clip);
	}


	public void play(string fileName) {
		if (!fileName.ToLower().EndsWith(".wav")) {
			fileName += ".wav";
		}
		Debug.Log ("path "+Application.persistentDataPath + myPath + fileName);
		if (File.Exists (Application.persistentDataPath + myPath + fileName)) {
			StartCoroutine ("LoadClip",fileName);				
		} else {
			Debug.Log (fileName + " No clip found");
			var filepath = Path.Combine(Application.persistentDataPath+ RecordController.myPath, fileName);
			Debug.Log (filepath);
		}
	}

	IEnumerator LoadClip(string fileName)
	{
		string path ="file://" +Application.persistentDataPath + myPath + fileName;
		Debug.Log ("try to  loded");
		WWW www;
		www = new WWW (path);
		yield return www;
		Debug.Log ("try to  loded"+ www.error);
		if (www.error == null) {
			if (www.GetAudioClip() != null) {
				Debug.Log ("audio loded");
				clip = www.GetAudioClip();
			}
			Debug.Log (path + " - couldn't be loaded..");
		}
			//clip = www.GetAudioClip(false, false, AudioType.WAV);
			//clip = www.audioClip;
			goAudioSource.clip = clip;
			goAudioSource.Play ();
			yield return null;
	}


	public void stopPlaying() {
		if(goAudioSource!= null && goAudioSource.isPlaying) {
			StopCoroutine ("LoadClip");
			goAudioSource.Stop ();
		}
	}



	public void delete(string fileName) 
	{
		string filePath = Application.persistentDataPath + myPath + fileName +"_1" + ".wav";

		// check if file exists
		if ( File.Exists( filePath )){
			string filePathMeta = Application.persistentDataPath + myPath + fileName +"_1" + ".meta";
			File.Delete( filePath );
			File.Delete( filePathMeta );
			RefreshEditorProjectWindow();
			if(onRecordingDeleted!= null) {
				onRecordingDeleted ();
			}
            Alphabet_UIController.isRecordedFileAvailable = false;

        }
    }

	public bool isFileAvailable(string fileName) {
		string filePath = Application.persistentDataPath+ myPath + fileName +"_1" + ".wav";
		// check if file exists
		if ( !File.Exists( filePath )){
			return false;
		}
		else{
			return true;
		}
	}

	void RefreshEditorProjectWindow() 
	{
		#if UNITY_EDITOR
		UnityEditor.AssetDatabase.Refresh();
		#endif
	}
}
