﻿using System.Collections;
using UnityEngine;

public class FlashCards_Animal_CanvasSwitch : MonoBehaviour {

	#region public variables
	     public GameObject  threeD_Canvas, ar_Canvas, ar_Camera, mGroundForShadow;
	#endregion

	#region delegate events
	public delegate void ThreeD_ModulesActive (bool status);
	public static event ThreeD_ModulesActive threeD_ModulesActive;
    public GameObject mPopUp;
	#endregion

	public void ARThreeD_CanvasOnOff(bool status)
	{
        Alphabet_ZoomInOut.instance.enabled = !status;
        StartCoroutine(ARThreePanel(status));
    }

	// switch ar and three canvas
	IEnumerator ARThreePanel(bool status)
	{
		yield return new WaitForSeconds(0.1f);

		LoadAlphabet_OnDetection.instance._3dMode = status;
		Alphabet_SoundManager.instance.StopIdleSound ();
		if (status)
		{
            mPopUp.SetActive(false);
			ar_Canvas.SetActive(!status);
			threeD_Canvas.SetActive(status);
            mGroundForShadow.SetActive(false);
			if (threeD_ModulesActive != null)
			{
				threeD_ModulesActive(status);
			}
            FlashCards_Animal_DinoInfoUI_Handler.instace.Handle3DLayout(true);

			FlashCards_Animal_DinoInfoUI_Handler.instace.dinoBtn_Parent.transform.GetComponent<RectTransform> ().offsetMax = Vector2.zero;
			FlashCards_Animal_DinoInfoUI_Handler.instace.dinoBtn_Parent.transform.GetComponent<RectTransform> ().offsetMin = Vector2.zero;
        }
        else
        {
            mGroundForShadow.SetActive(true);
            threeD_Canvas.SetActive(status);
			ar_Canvas.SetActive(!status);
            var aTransform = FlashCards_Animal_DinoInfoUI_Handler.instace.ThreeDView.transform;

            for(int i = 0; i < aTransform.childCount; i++)
            {
                Destroy(aTransform.GetChild(i).gameObject);
            }

            FlashCards_Animal_DinoInfoUI_Handler.instace.Handle3DLayout(false);
        }


    }
	}
