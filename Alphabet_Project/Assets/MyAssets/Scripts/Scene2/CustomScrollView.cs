﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

public class CustomScrollView : MonoBehaviour
{

	//--------------------------------------------------------//
	[Header ("Controllers")]
	[Range (0, 500)]
	public int itemOffset;
	[Range (0f, 20f)]
	public float snapSpeed;
	[Range (0f, 10f)]
	public float scaleOffset;
	[Range (1f, 20f)]
	public float scaleSpeed;

	//--------------------------------------------------------//
	[Header ("Other Objects")]
	public ScrollRect scrollRect;


	//--------------------------------------------------------//
	public List<GameObject> instItem;
	private List<Vector2> itemPos;
	private RectTransform contentRect;
	private Vector2 contentVector;
	private int selectedPanID;
	private bool isScrolling;
	private bool isCatalogCreated;
	private int numOfChildsTotal = 0, numOfChildsCurrent = 0;

	#region Event

	public delegate void CenterItemInfo (int scrollviewPosition);

	public static event CenterItemInfo centerItemInfo;

	#endregion

	private void Start ()
	{
		contentRect = GetComponent<RectTransform> ();
		instItem = new List<GameObject> ();
		itemPos = new List<Vector2> ();
	}

	public void AddItems ()
	{
		isCatalogCreated = false;
		for (int i = 0; i < this.transform.childCount; i++) {
			
			this.transform.GetChild (i).gameObject.name = "";
			instItem.Add (this.transform.GetChild (i).gameObject);

			if (i > 0)
				instItem [i].transform.localPosition = new Vector2 (instItem [i - 1].transform.localPosition.x + instItem [i].GetComponent<RectTransform> ().sizeDelta.x + itemOffset, 300);
			else {
				
				instItem [i].transform.localPosition = new Vector2 (0f, 300);
			}
			itemPos.Add (-instItem [i].transform.localPosition);
			//Debug.Log (i+" : "+itemPos[i].y);
		}
		numOfChildsCurrent = numOfChildsTotal;
		isCatalogCreated = true;
	}

	private void FixedUpdate ()
	{
		numOfChildsTotal = this.transform.childCount;

		if (isCatalogCreated && numOfChildsTotal == numOfChildsCurrent && instItem [0].gameObject != null) {
			if ((contentRect.anchoredPosition.x >= itemPos [0].x  || contentRect.anchoredPosition.x <= itemPos [itemPos.Count - 1].x) && !isScrolling)
				scrollRect.inertia = false;
			float nearestPos = float.MaxValue;
			for (int i = 0; i < instItem.Count; i++) {
				float distance = Mathf.Abs (contentRect.anchoredPosition.x - itemPos [i].x);
				if (distance < nearestPos) {
					nearestPos = distance;
					selectedPanID = i;
				}
				float scale = Mathf.Clamp (1 / (distance / itemOffset) * scaleOffset, 0.5f, 1f);
				foreach (Transform trans in instItem[i].GetComponentsInChildren<Transform>()) {
					if (trans.name == "imageAlpha")
					{
						trans.GetComponent<Image>().color = ImageNewAlpha(trans.GetComponent<Image>(), distance);
						if (trans.GetComponent<Image>().color.a < 0.1f)
						      centerItemInfo (i);
					}
				}
				Vector2 itemScale = new Vector2 (Mathf.SmoothStep (instItem [i].transform.localScale.x, scale + 0.3f, scaleSpeed * Time.fixedDeltaTime), Mathf.SmoothStep (instItem [i].transform.localScale.y, scale + 0.3f, scaleSpeed * Time.fixedDeltaTime));
				instItem [i].transform.localScale = itemScale;
				RectTransform rt = instItem [i].GetComponent<RectTransform> ();
				rt.sizeDelta = new Vector2 (rt.sizeDelta.x, ItemNewHeight (distance));
			}
			float scrollVelocity = Mathf.Abs (scrollRect.velocity.x);
			if (scrollVelocity < 200 && !isScrolling)
				scrollRect.inertia = false;
			if (isScrolling || scrollVelocity > 200)
				return;
			contentVector.x = Mathf.SmoothStep (contentRect.anchoredPosition.x, itemPos [selectedPanID].x, snapSpeed * Time.fixedDeltaTime);
			contentRect.anchoredPosition = contentVector;
		}
		//else if (!isCatalogCreated && numOfChildsTotal == numOfChildsCurrent) {

		//}
		else if (numOfChildsTotal != numOfChildsCurrent) {
			if (instItem != null) {
				instItem.Clear ();
				itemPos.Clear ();
			}
			AddItems ();
		} else if (instItem [0].gameObject == null) {
			if (instItem != null) {
				instItem.Clear ();
				itemPos.Clear ();
			}
			AddItems ();
		}
	}

	public Color ImageNewAlpha (Image image, float distanceFromCenter)
	{
		float alphaValue = (distanceFromCenter / 255) * 0.75f;
		if (alphaValue > 0.8f)
			alphaValue = 0.8f;
		Color c = image.color;
		c.a = alphaValue;
		return c;
	}

	public float ItemNewHeight (float distanceFromCenter)
	{

		float newAxis;
		if (distanceFromCenter > 0f)
			newAxis = ((distanceFromCenter / 3) - (100 / distanceFromCenter));
		else
			newAxis = 0f;

		if (newAxis < 0)
			newAxis = 0f;

		return newAxis;
	}
}