﻿using UnityEngine;
using Vuforia;

public class ARCameraController : MonoBehaviour
{

	#region public variable

	public bool isARModule, isSkyboxOn;
	public static ARCameraController instance;

	#endregion

	void Start ()
	{
		Debug.Log ("start called " + isARModule);
		instance = this;
		if (isARModule)
			AR_ModulesActivate();
		else
			ThreeD_ModulesActivate ();
	}

	void Update ()
	{

		if (isARModule && GetComponent <VuforiaBehaviour> ().enabled == false) {
			//VuforiaRuntime.Instance.InitVuforia ();
			if (isSkyboxOn)
				GetComponent <Camera> ().clearFlags = CameraClearFlags.Skybox;
			else
				GetComponent <Camera> ().clearFlags = CameraClearFlags.SolidColor;
		//	GetComponent <VuforiaBehaviour> ().enabled = true;
		}
	}

	public	void ThreeD_ModulesActivate ()
	{
		Debug.Log("three modules deactivate ");
		if (GetComponent <VuforiaBehaviour> () != null && GetComponent <VuforiaBehaviour> ().enabled == true) {
			//VuforiaRuntime.Instance.Deinit ();
			Debug.Log ("deactivate vuforia");
			if (isSkyboxOn)
				GetComponent <Camera> ().clearFlags = CameraClearFlags.Skybox;
			else
				GetComponent <Camera> ().clearFlags = CameraClearFlags.SolidColor;
            GetComponent<VuforiaBehaviour>().enabled = false;
        }

	}

	public void AR_ModulesActivate ()
	{
		Debug.Log("AR modules called activated");
		if (GetComponent <VuforiaBehaviour> ().enabled == false) {
			//VuforiaRuntime.Instance.InitVuforia ();
			//if (isSkyboxOn)
			//	GetComponent <Camera> ().clearFlags = CameraClearFlags.Skybox;
			//else
				GetComponent <Camera> ().clearFlags = CameraClearFlags.SolidColor;
			GetComponent <VuforiaBehaviour> ().enabled = true;
		}

	}
}
