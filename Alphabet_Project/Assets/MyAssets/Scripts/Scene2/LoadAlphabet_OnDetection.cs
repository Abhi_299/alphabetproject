﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadAlphabet_OnDetection : MonoBehaviour {

    public static LoadAlphabet_OnDetection instance;

    #region Public variables
    public enum SceneType { AR, ThreeD, None };
    public GameObject[] alphabetModels;
    public DefaultAlphabetSpellManager alphabetSounds;
    public Transform _3DView;
    #endregion

    public delegate void OnModelLoaded(string name, Transform _transform, bool status, SceneType sceneType);
    public static event OnModelLoaded isModelLoaded;


	internal bool _3dMode;
    #region Private variable
    Transform model;
    SceneType sceneType;
    bool isModelDetected;
    Coroutine loadModelCoroutine;
    #endregion

    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            instance = this;
            sceneType = SceneType.None;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void OnEnable()
    {
        TrackableEventHandler.onImageTargetDetected += TrackableEventhandler_OnTargetImageDetected;
    }

    void OnDisable()
    {
        TrackableEventHandler.onImageTargetDetected -= TrackableEventhandler_OnTargetImageDetected;
    }

    void TrackableEventhandler_OnTargetImageDetected(string arg, Transform _transform, bool isFound)
    {
        isModelDetected = isFound;
        if (isFound)
        {
            loadModelCoroutine = StartCoroutine(OnTrackingFound(arg, _transform, true));
        }
        else
        {
            if (loadModelCoroutine != null) {
                StopCoroutine(loadModelCoroutine);
            }
            OnTrackingLost(_transform);
            if (isModelLoaded != null) {
                isModelLoaded(arg,_transform,false,SceneType.None);
            }
        }
    }


    public IEnumerator OnTrackingFound(string nameData, Transform _transform, bool status)
    {
        yield return new WaitForEndOfFrame();

      
        if (status)
        {
//            Debug.Log("Model to Load- "+nameData);

            for (int i = 0; i < alphabetModels.Length; i++)
            {
                var currAlphabet_ModelController = alphabetModels[i].transform.GetComponent<Alphabet_ModelController>().modelInfoObject;
				var modelName = alphabetModels[i].name;
                if (nameData.ToLower() == modelName.ToLower())
                {

					Debug.Log("Model to Load- "+nameData);
					Debug.Log("Prefab to Load- "+modelName);

                    model = alphabetModels[i].transform;
                    model = Instantiate(model, _transform).transform;
                    model.name = nameData;
                      
                    Debug.Log("Is Model Loaded Tracking Found");

                    if (isModelDetected)
                    {
                        isModelLoaded(modelName, _transform, true, SceneType.AR);
                    }
                    else
                    {
                        isModelLoaded(modelName, model.transform, true, SceneType.ThreeD);
                    }

                    Alphabet_UIController.instance.alphabetBtnTxt.text = currAlphabet_ModelController.alphabetCharacter;

                    Alphabet_SoundManager.instance.SetAlphabet_AudioClip(alphabetSounds.letters.Find(item => item.letter.ToLower() == currAlphabet_ModelController.alphabetCharacter.ToString().ToLower()).spell);

					if (currAlphabet_ModelController.idleSound != null) {
						Alphabet_SoundManager.instance.SetIdleSound (currAlphabet_ModelController.idleSound);
						Alphabet_SoundManager.instance.idleSoundPlayer.volume = 1;
					} else {
						Alphabet_SoundManager.instance.idleSoundPlayer.volume = 0;
					}
                    Alphabet_PlayRecordDeleteHandler.instance.isMarkerDetected = true;
					Alphabet_PlayRecordDeleteHandler.instance.SetSpelling(model.GetComponent<Alphabet_ModelController>());
                    Alphabet_ZoomInOut.instance.currModel = model.gameObject;
                    Alphabet_ZoomInOut.instance.isModlDetected = true;

					if (Alphabet_PlayRecordDeleteHandler.instance.CheckRecordingAvalable())
					{
						Alphabet_UIController.instance.deleteBtn.interactable = true;
						Alphabet_UIController.instance.recordBtn.interactable = false;
					}
					else
					{
						Alphabet_UIController.instance.deleteBtn.interactable = false;
						Alphabet_UIController.instance.recordBtn.interactable = true;
					}
                    break;
                }
            }
            Alphabet_UIController.instance.centerMenu.SetActive(true);
            Alphabet_UIController.instance.alphabetBtn.SetActive(true);
        } else {
            isModelLoaded(nameData,_transform,false,SceneType.None);
        }
       
 
    }

    void OnTrackingLost(Transform _transform)
    {
        Alphabet_ZoomInOut.instance.isModlDetected = false;

        if (_transform.childCount > 0)
        {
            for (int i = 0; i < _transform.childCount; i++)
            {
                Destroy(_transform.GetChild(i).gameObject);
            }
        }
        if (Alphabet_UIController.instance.centerMenu != null)
        {
            Alphabet_UIController.instance.centerMenu.SetActive(false);
        }
        if (Alphabet_UIController.instance.alphabetBtn != null)
        {
            Alphabet_UIController.instance.alphabetBtn.SetActive(false);
        }
        Alphabet_PlayRecordDeleteHandler.instance.isMarkerDetected = false;
        StopCoroutine(Alphabet_PlayRecordDeleteHandler.instance.TriggerSpelling());
        Alphabet_PlayRecordDeleteHandler.instance.ResetSpelling();
        Alphabet_SoundManager.instance.StopIdleSound();
        if (Alphabet_SoundManager.instance.idleSoundPlayer != null)
        {
            Alphabet_SoundManager.instance.idleSoundPlayer.clip = null;
        }

    }


}
