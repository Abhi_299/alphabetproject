﻿ using System.Collections;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI; 

public class ScreenShotHandler : MonoBehaviour {
    #region Private Variable
    private int screenShotTimer = 3;
    private string path = null, screenShotName;
    #endregion

    #region Public Variable
    public Text txt_ScreenCaptureTimer;
    public Image currentSelectedPhoto;
    public Button shareBtn;
    public GameObject popUpScreenShotSaved, screenShotBtn, screenShotShown;
    #endregion


    // Use this for initialization
    void Start () {
        path = LinksAndNames.PathToImages;
    }

    public void CaptureScreenShot()
    {
        if (screenShotBtn != null)
            screenShotBtn.GetComponent<Button>().interactable = false;
        StartCoroutine(SaveScreenShot());
    }

    IEnumerator SaveScreenShot() {
        if (screenShotTimer > 0)
        {
            Alphabet_SoundManager.instance.PlayTimerSound();
            txt_ScreenCaptureTimer.gameObject.SetActive(true);
            txt_ScreenCaptureTimer.text = "" + screenShotTimer;
            screenShotTimer -= 1;
            yield return new WaitForSeconds(1);
            StartCoroutine(SaveScreenShot());
        }
        else
        {
            screenShotName = "";
            Alphabet_SoundManager.instance.PlayClickSound();
            txt_ScreenCaptureTimer.gameObject.SetActive(false);
            screenShotTimer = 3;
            yield return new WaitForEndOfFrame();

            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string imageName = DateTime.Now.ToFileTime().ToString();
                screenShotName = path + imageName;
                RenderTexture rt = new RenderTexture(Screen.width, Screen.height, 100, RenderTextureFormat.ARGB32);
                rt.useMipMap = false;
                rt.antiAliasing = 1;
                RenderTexture.active = rt;
                Camera.main.targetTexture = rt;
                Texture2D shot = new Texture2D(Screen.width, Screen.height, TextureFormat.ARGB32, false);
                Camera.main.Render();
                shot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
                shot.Apply();
                byte[] bytes = shot.EncodeToPNG();
                File.WriteAllBytes(screenShotName + ".png", bytes);
                if (popUpScreenShotSaved != null)
                {
                    popUpScreenShotSaved.SetActive(true);
                    popUpScreenShotSaved.GetComponent<Animator>().Play("");
                    //popUpScreenShotSaved.GetComponentInChildren<Text>().text = "Screenshot Saved!";
                }
                StartCoroutine(ScreenshotShownPopUp(shot));
                bytes = null;
                shot = null;
                Camera.main.targetTexture = null;
                RenderTexture.active = null;
                Destroy(rt);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }
        if (screenShotBtn != null)
        {
            screenShotBtn.GetComponent<Button>().interactable = true;
        }
    }

    IEnumerator ScreenshotShownPopUp(Texture2D _texture)
    {
        yield return new WaitForEndOfFrame();
        if (_texture != null)
        {
            currentSelectedPhoto.sprite = Sprite.Create(_texture, new Rect(0, 0, _texture.width, _texture.height), new Vector2(0.2f, 0.2f), 100);
            yield return new WaitForEndOfFrame();
            screenShotShown.SetActive(true);

            shareBtn.GetComponent<Button>().onClick.RemoveAllListeners();
            shareBtn.GetComponent<Button>().onClick.AddListener(() => { ShareScreenShot(screenShotName); });
            _texture = null;
        }
    }

    public void ShareScreenShot(string fileName) {
        popUpScreenShotSaved.SetActive(false);
        StartCoroutine(Share(fileName));
    }

    IEnumerator Share(string fileName) {
        yield return new WaitForEndOfFrame();
        //instantiate the class Intent
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");

        //instantiate the object Intent
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

        //call setAction setting ACTION_SEND as parameter
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

        //instantiate the class Uri
        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");

        //instantiate the object Uri with the parse of the url's file
        string destination = screenShotName;
        AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + destination);

        //call putExtra with the uri object of the file
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);

        //set the type of file
        intentObject.Call<AndroidJavaObject>("setType", "image/*");

        //instantiate the class UnityPlayer
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        //instantiate the object currentActivity
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

        //call the activity with our Intent
        currentActivity.Call("startActivity", intentObject);
    }

    public void On_BackBtn() {
        if (screenShotShown != null) {
            screenShotShown.SetActive(false);
            if (currentSelectedPhoto != null) {
                currentSelectedPhoto.sprite = null;
            }
        }
        popUpScreenShotSaved.SetActive(false);
    }

    void OnDisable()
    {
        if (!string.IsNullOrEmpty(path))
        {
            path = null;
        }

        if (screenShotBtn != null)
        {
            screenShotBtn.GetComponent<Button>().interactable = true;
            screenShotBtn = null;
            screenShotName = "";
            path = null;
        }
    }
}
