﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Alphabet Data Info", menuName = "WizAr/Create alphabet data manager")]
public class Alphabet_Data_ScriptableObject : ScriptableObject {

    public List<Object_Data> alphabetInfo;
}

[System.Serializable]
public class Object_Data
{
    public string _character;
    public List<Alphabet_Data> alphabetModel;
}

[System.Serializable]
public class Alphabet_Data
{
    public GameObject rightModel;
    public GameObject wrongModel;
    public Sprite icon;
    public bool isTopFalling;
}
