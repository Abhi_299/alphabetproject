﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F_Alpha_ActivityObjectMover : MonoBehaviour {

    public bool topFallingObject;
    public float speed;
    public float endPoint;
    float movePosition;

	// Use this for initialization
	void OnEnable () {

        movePosition = -endPoint;
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (topFallingObject)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, 0, movePosition);
            if (movePosition > endPoint)
            {
                movePosition -= speed;
            }
            else {
                for (int i = 0; i < transform.childCount; i++)
                {
                    transform.GetChild(i).gameObject.SetActive(false);
                    transform.GetChild(i).SetParent(null);
                }
                gameObject.SetActive(false);
            }
        }
        else
        {
            transform.localPosition = new Vector3(movePosition, 0, transform.localPosition.z);
            if (movePosition < endPoint)
            {
                movePosition += speed;
            }
            else
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    transform.GetChild(i).gameObject.SetActive(false);
                    transform.GetChild(i).SetParent(null);
                }
                gameObject.SetActive(false);
            }
        }
	}
}
