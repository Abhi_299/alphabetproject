﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashcardAlphabet_ActivityManager : MonoBehaviour {

    public Transform _camera;
    public Activity_ScriptableObject activityAssetManager;
    public Text timeCountTxt;
    public float timeCounter;
    public GameObject rightEffect, wrongEffect;
    public Image[] scorePanelIcon, gameOverIcon;
    public Text[] scoreTxt, gameOverScoreTxt;
    public GameObject gameOverPanel, startPanel;
    public AudioSource clock_Sound, entry_Sound, right_Sound;

    int[] scoreCount = new int[2];

    [Header("Life Line Icon")]
    public Image[] lifeLineIcon;
    private int lifeCount;

    [Header("Top Targets Position")]
    public List<GameObject> topTargets;

    [Header("Side Targets Positions")]
    public List<GameObject> sideTargets;

    [Header("Alphabet Data")]
    public List<ObjectData> objectData;

    private List<GameObject> topObjects, sideObjects;
    private int side_RandomIndex, top_RandomIndex;
    private int sideTarget_Index, topTarget_index;
    Coroutine particleCoroutine;
    bool isGameOver, isGameStart;

    private void OnEnable()
    {
        InputTouchHit.OnScreenTouch += Hit_Object;
        Invoke("SetCamera", 0.5f);
    }

    private void OnDisable()
    {
        InputTouchHit.OnScreenTouch -= Hit_Object;
    }

    // Use this for initialization
    void Start () {
        if (timeCounter == 0) {
            timeCounter = 20;
        }
        topObjects = new List<GameObject>();
        sideObjects = new List<GameObject>();
        DisableMoving_Object();

        side_RandomIndex = -1;
        top_RandomIndex = -1;
        isGameOver = false;
        isGameStart = false;
        lifeCount = 3;

        if (_camera.Find("BackgroundPlane") != null) {
            _camera.Find("BackgroundPlane").gameObject.SetActive(false);
        }
        LoadSelectedData();

        startPanel.SetActive(true);
    }

    public void Close_StartPanel() {
        startPanel.SetActive(false);
        isGameStart = true;

        if (topObjects.Count > 0)
        {
            StartCoroutine(MoveTopObject());
        }

        if (sideObjects.Count > 0)
        {
            StartCoroutine(MoveSideObject());
        }
    }

    void SetCamera() {
        _camera.transform.localEulerAngles = new Vector3(-38, 0, 0);
    }

    private void Update()
    {
        if (timeCounter > 0 && isGameStart && lifeCount > 0)
        {
            timeCounter -= Time.fixedDeltaTime;
            timeCountTxt.text = (int)timeCounter + "";
        }

        if(timeCounter > 0 && timeCounter < 5 && isGameStart && lifeCount > 0)
        {
            if (!clock_Sound.isPlaying)
            {
                clock_Sound.Play();
            }
        }
        else
        {
            clock_Sound.Stop();
        }
    }

    IEnumerator MoveTopObject() {
        while (timeCounter > 0) {

            if (lifeCount == 0) {
                break;
            }

            TopObject_Mover();
            yield return new WaitForEndOfFrame();
            topTargets[topTarget_index].SetActive(true);
            topObjects[top_RandomIndex].SetActive(true);
            yield return new WaitForSeconds(2.3f);
        }

        if (!isGameOver) {
            OnGameOver();
        }
    }

    void TopObject_Mover() {
        int i = Random.Range(0, 4);
        while (i == top_RandomIndex)
        {
            i = Random.Range(0, 4);
        }
        top_RandomIndex = i;

        i = Random.Range(0, 4);
        while (i == topTarget_index)
        {
            i = Random.Range(0, 4);
        }
        topTarget_index = i;

        while (topTargets[topTarget_index].transform.childCount > 0)
        {
            Transform _child = topTargets[topTarget_index].transform.GetChild(0);
            _child.SetParent(null);
            _child.gameObject.SetActive(false);
        }

        topObjects[top_RandomIndex].transform.SetParent(topTargets[topTarget_index].transform);
        topObjects[top_RandomIndex].transform.localPosition = Vector3.zero;
        topObjects[top_RandomIndex].transform.localEulerAngles = Vector3.zero;
        topObjects[top_RandomIndex].transform.localScale = Vector3.one;

        entry_Sound.Stop();
        entry_Sound.Play();

        topTargets[topTarget_index].transform.localEulerAngles = topObjects[top_RandomIndex].transform.GetComponent<F_Alpha_ActivityTransformHandler>().initialRotation;

        float f = topObjects[top_RandomIndex].transform.GetComponent<F_Alpha_ActivityTransformHandler>().initialSize;
        topTargets[topTarget_index].transform.localScale = new Vector3(f, f, f);
    }

    IEnumerator MoveSideObject()
    {
        while (timeCounter > 0)
        {
            if (lifeCount == 0)
            {
                break;
            }

            SideObject_Mover();
            yield return new WaitForEndOfFrame();
            sideTargets[sideTarget_Index].SetActive(true);
            sideObjects[side_RandomIndex].SetActive(true);
            if (sideObjects[side_RandomIndex].transform.GetComponentInChildren<Animator>() != null &&
                sideObjects[side_RandomIndex].transform.GetComponentInChildren<Animator>().GetBool("move") != null)
            {

                sideObjects[side_RandomIndex].transform.GetComponentInChildren<Animator>().SetBool("move", true);
                Debug.Log("Anim Move");
            }
            yield return new WaitForSeconds(3.5f);
        }

        if (!isGameOver)
        {
            OnGameOver();
        }
    }

    void SideObject_Mover() {
        int i = Random.Range(0, 4);
        while (i == side_RandomIndex)
        {
            i = Random.Range(0, 4);
        }
        side_RandomIndex = i;

        i = Random.Range(0, 3);
        while (i == sideTarget_Index)
        {
            i = Random.Range(0, 3);
        }
        sideTarget_Index = i;

        sideObjects[side_RandomIndex].transform.SetParent(sideTargets[sideTarget_Index].transform);
        sideObjects[side_RandomIndex].transform.localPosition = Vector3.zero;
        sideObjects[side_RandomIndex].transform.localEulerAngles = Vector3.zero;
        sideTargets[sideTarget_Index].transform.localEulerAngles = sideObjects[side_RandomIndex].transform.GetComponent<F_Alpha_ActivityTransformHandler>().initialRotation;
        entry_Sound.Stop();
        entry_Sound.Play();
    }

    void DisableMoving_Object() {
        for (int i = 0; i < topTargets.Count; i++)
        {
            topTargets[i].SetActive(false);
        }

        for (int i = 0; i < sideTargets.Count; i++)
        {
            sideTargets[i].SetActive(false);
        }
    }

    //To generate selected alphabet model on starting scene
    public void LoadSelectedData() {
        for (int i = 0; i < objectData.Count; i++)
        {
            if (activityAssetManager.current_Charater.ToLower() == objectData[i]._character.ToLower())
            {
                for (int j = 0; j < objectData[i].alphabetModel.Count; j++)
                {
                    if (objectData[i].alphabetModel[j].isTopFalling)
                    {
                        Generate_TopObject(i, j);
                    }
                    else {
                        Generate_SideObject(i, j);
                    }
                }
                break;
            }
        }
    }

    //To generate top falling object
    void Generate_TopObject(int objDataIndex, int alphabetModelIndex) {

        scorePanelIcon[alphabetModelIndex].sprite = objectData[objDataIndex].alphabetModel[alphabetModelIndex].icon;
        gameOverIcon[alphabetModelIndex].sprite = objectData[objDataIndex].alphabetModel[alphabetModelIndex].icon;

        GameObject Obj = Instantiate(objectData[objDataIndex].alphabetModel[alphabetModelIndex].rightModel);
        Obj.GetComponent<F_Alpha_ActivityTransformHandler>().scoreIndex = alphabetModelIndex;

        topObjects.Add(Obj);
        topObjects.Add(Instantiate(objectData[objDataIndex].alphabetModel[alphabetModelIndex].wrongModel));

        if (objectData[objDataIndex].alphabetModel[0].isTopFalling != objectData[objDataIndex].alphabetModel[1].isTopFalling)//If both model are not top falling objects then create another copy of objects
        {
            Obj = Instantiate(objectData[objDataIndex].alphabetModel[alphabetModelIndex].rightModel);
            Obj.GetComponent<F_Alpha_ActivityTransformHandler>().scoreIndex = alphabetModelIndex;

            topObjects.Add(Obj);
            topObjects.Add(Instantiate(objectData[objDataIndex].alphabetModel[alphabetModelIndex].wrongModel));
        }

        for (int i = 0; i < topObjects.Count; i++)
        {
            topObjects[i].SetActive(false);
        }
    }

    //To generate side moving object
    void Generate_SideObject(int objDataIndex, int alphabetModelIndex)
    {
        scorePanelIcon[alphabetModelIndex].sprite = objectData[objDataIndex].alphabetModel[alphabetModelIndex].icon;
        gameOverIcon[alphabetModelIndex].sprite = objectData[objDataIndex].alphabetModel[alphabetModelIndex].icon;

        objectData[objDataIndex].alphabetModel[alphabetModelIndex].rightModel.GetComponent<F_Alpha_ActivityTransformHandler>().scoreIndex = alphabetModelIndex;

        GameObject Obj = Instantiate(objectData[objDataIndex].alphabetModel[alphabetModelIndex].rightModel);
        Obj.GetComponent<F_Alpha_ActivityTransformHandler>().scoreIndex = alphabetModelIndex;

        sideObjects.Add(Obj);
        sideObjects.Add(Instantiate(objectData[objDataIndex].alphabetModel[alphabetModelIndex].wrongModel));

        if (objectData[objDataIndex].alphabetModel[0].isTopFalling != objectData[objDataIndex].alphabetModel[1].isTopFalling)//If both model are not side moving objects
        {
            Obj.GetComponent<F_Alpha_ActivityTransformHandler>().scoreIndex = alphabetModelIndex;

            sideObjects.Add(Obj);
            sideObjects.Add(Instantiate(objectData[objDataIndex].alphabetModel[alphabetModelIndex].wrongModel));
        }

        for (int i = 0; i < sideObjects.Count; i++)
        {
            sideObjects[i].SetActive(false);
        }
    }

    #region Hitting object score manager

    public void Hit_Object(RaycastHit hit) {
        if (hit.transform.GetComponentInChildren<Alphabet_ModelController>().modelInfoObject.alphabetCharacter.ToLower() == activityAssetManager.current_Charater.ToLower())
        {
            hit.transform.gameObject.SetActive(false);

            int index = hit.transform.GetComponent<F_Alpha_ActivityTransformHandler>().scoreIndex;
            scoreCount[index]++;
            scoreTxt[index].text = "" + scoreCount[index];
            gameOverScoreTxt[index].text = "" + scoreCount[index];

            rightEffect.transform.position = hit.transform.position;
            if (particleCoroutine != null) {
                StopCoroutine(particleCoroutine);
            }
            particleCoroutine = StartCoroutine(Enable_ParticleEffect(true));
            //Enable right effect
        }
        else
        {
            lifeCount--;
            lifeLineIcon[lifeCount].color = new Color(1, 0.55f, 0, .25f);

            hit.transform.gameObject.SetActive(false);
            wrongEffect.transform.position = hit.transform.position;
            if (particleCoroutine != null)
            {
                StopCoroutine(particleCoroutine);
            }
            particleCoroutine = StartCoroutine(Enable_ParticleEffect(false));
            //Enable wrong effect
        }
        Debug.Log("Hit object");
    }

    IEnumerator Enable_ParticleEffect(bool isRight) {
        rightEffect.SetActive(false);
        wrongEffect.SetActive(false);
        if (isRight)
        {
            rightEffect.SetActive(true);
            right_Sound.Stop();
            right_Sound.Play();
        }
        else
        {
            wrongEffect.SetActive(true);
            Handheld.Vibrate();
        }
        yield return new WaitForSeconds(1);
        rightEffect.SetActive(false);
        wrongEffect.SetActive(false);
    }

    #endregion

    void OnGameOver() {
        isGameOver = true;
        gameOverPanel.SetActive(true);
        DisableMoving_Object();
    }
}


[System.Serializable]
public class ObjectData {
    public string _character;
    public List<Alphabet_Model> alphabetModel;
}

[System.Serializable]
public class Alphabet_Model {
    public GameObject rightModel;
    public GameObject wrongModel;
    public Sprite icon;
    public bool isTopFalling;
}
