﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F_Alpha_ActivityTransformHandler : MonoBehaviour {

    public Vector3 initialPosition, initialRotation;
    public float initialSize;
    internal int scoreIndex;

    [Header("GamePlay 2")]
    public Vector3 rotation_G2;
    public float size_G2 = 1;

    [Header("3D trasform")]
    public Vector3 rotation_3D;
    public float size_3D = 1;

    // Use this for initialization
    void Start () {
		
	}

}
