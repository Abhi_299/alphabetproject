﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDetection_Manager : MonoBehaviour {

    public GameObject gamePlayBtn;
    public GameObject instPanel;
    public Activity_ScriptableObject activityAssetManager;

    [Header("Inst panel msg")]
    public string placeTwoCard_Msg;
    public string placeOneCard_Msg;
    public string placeSameCard_Msg;

    private List<string> detectedChar;
    int detectedCount;

	// Use this for initialization
	void Start () {
        detectedChar = new List<string>();
	}

    private void OnEnable()
    {
        LoadModelOnDetection.isModelLoaded += On_Loading;
    }

    private void OnDisable()
    {
        LoadModelOnDetection.isModelLoaded -= On_Loading;
    }

    public void On_Loading(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType) {
        VuforiaUnity.SetHint(VuforiaUnity.VuforiaHint.HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, 2);
        StartCoroutine(OnModelLoading(_transform, status));
    }

    IEnumerator OnModelLoading(Transform _transform, bool status)
    {
        if (!status) {
            if (detectedCount > 0) {
                detectedCount--;
                detectedChar.Remove(_transform.GetComponentInChildren<Alphabet_ModelController>().modelInfoObject.alphabetCharacter);
            }

            if (detectedCount == 0 && detectedChar != null) {
                detectedChar.Clear();
            }
            if (gamePlayBtn != null)
            {
                gamePlayBtn.SetActive(false);
            }
        }

        yield return new WaitForEndOfFrame();

        if (status) {
            detectedCount++;
            detectedChar.Add(_transform.GetComponentInChildren<Alphabet_ModelController>().modelInfoObject.alphabetCharacter);
        }

        if (detectedCount > 1)
        {
            if (detectedChar[0].ToLower() == detectedChar[1].ToLower())
            {
                gamePlayBtn.SetActive(true);
                instPanel.SetActive(false);
            }
            else {
                instPanel.GetComponentInChildren<Text>().text = placeSameCard_Msg;
                instPanel.SetActive(true);
            }
        }
        else if (detectedCount > 0)
        {
            instPanel.GetComponentInChildren<Text>().text = placeOneCard_Msg;
            instPanel.SetActive(true);
        }
        else if(detectedCount == 0){
            instPanel.GetComponentInChildren<Text>().text = placeTwoCard_Msg;
            instPanel.SetActive(true);
        }


    }

    public void Click_GamePlayBtn(int index) {
        activityAssetManager.current_Charater = detectedChar[0];
        StartCoroutine(Load_Level(index));
    }

    IEnumerator Load_Level(int index) {
        yield return new WaitForEndOfFrame();
        SceneManager.LoadScene(index);
    }

}
