﻿using UnityEngine;

[CreateAssetMenu(fileName = "Character", menuName = "WizAr/Create Character Manager", order = 1)]
public class FlashCard_CharacterScriptableObject : ScriptableObject {

    public string characterName;
}
