﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadModel_OnDetection : MonoBehaviour {

    #region Public variables
    public GameObject[] alphabetModels;
    #endregion

    #region Private variable
    Transform model;
    #endregion

    // Use this for initialization
    void Start () {
		
	}

    void OnEnable() {
        TrackableEventHandler.onImageTargetDetected += TrackableEventhandler_OnTargetImageDetected;
    }

    void OnDisable() {
        TrackableEventHandler.onImageTargetDetected -= TrackableEventhandler_OnTargetImageDetected;
    }

    void TrackableEventhandler_OnTargetImageDetected(string arg,Transform _transform,bool isFound) {

        if (isFound) {
            OnTrackingFound(arg,_transform);
        }
        else {
            OnTrackingLost(_transform);
        }
    }


    void OnTrackingFound(string nameData, Transform _transform) {

        for (int i = 0; i < alphabetModels.Length; i++)
        {
            var modelName = alphabetModels[i].transform.GetComponent<FlashCard_CharacterController>().charInfoObject.characterName;     
            if (nameData.ToLower() == modelName.ToLower()) {
                model = alphabetModels[i].transform;
                model = Instantiate(model, _transform).transform;
                model.name = nameData;
            }
        }
        Alphabet_UIController.instance.centerMenu.SetActive(true);
       
    }

    void OnTrackingLost(Transform _transform) {

        if (_transform.childCount > 0) {
            for (int i = 0; i < _transform.childCount; i++)
            {
                Destroy(_transform.GetChild(i).gameObject);
            }
        }
        Alphabet_UIController.instance.centerMenu.SetActive(false);

    }
	
}
