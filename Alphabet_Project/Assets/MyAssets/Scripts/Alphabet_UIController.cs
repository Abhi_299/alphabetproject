﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Alphabet_UIController : MonoBehaviour {

    public static Alphabet_UIController instance;

    public GameObject arCanvas;
    public GameObject centerMenu;
    public GameObject alphabetBtn;
    public GameObject languageSelectionPanel;
    public GameObject spellCharacterParent, screenShotTimer;
    public Button deleteBtn, playBtn, recordBtn,captureBtn,backBtn,settingBtn;
    public GameObject recordingMsgPanel;
    public Text alphabetBtnTxt;
    public GameObject popUpActivityComplete;

    internal static bool isRecordedFileAvailable;
	// Use this for initialization
	void Awake () {
        instance = this;
//        SetBtnIntraction(true);

//		CheckRecordSoundAvailablity ();

    }

    void OnEnable() {
        RecordController.onRecordingSaved += HandleOnRecordingSaved;
        RecordController.onRecordingDeleted += HandleOnRecordingDelete;
    }

    void OnDisable() {
        RecordController.onRecordingSaved -= HandleOnRecordingSaved;
        RecordController.onRecordingDeleted -= HandleOnRecordingDelete;
    }

    public void ScreenShotUIManager(bool status) {
        arCanvas.SetActive(status);
        CancelInvoke("TurnOnUIAfterImageClick");
        Invoke("TurnOnUIAfterImageClick", 4);
    }

    void TurnOnUIAfterImageClick() {
        ScreenShotUIManager(true);
    }

    public void SetBtnIntraction(bool status) {
        playBtn.interactable = status;
        captureBtn.interactable = status;
        backBtn.interactable = status;
        settingBtn.interactable = status;
        if (!status)
        {
            deleteBtn.interactable = status;
            recordBtn.interactable = status;
        }
        else
        {
			if (Alphabet_PlayRecordDeleteHandler.instance.CheckRecordingAvalable())
			{
				deleteBtn.interactable = true;
				recordBtn.interactable = false;
			}
			else
			{
				deleteBtn.interactable = false;
				recordBtn.interactable = true;
			}
        }
    }

    public void RecordingBtnClick() {
        Alphabet_SoundManager.instance.CancelInvoke("AfterPlayingSound");
        Alphabet_SoundManager.instance.isSoundPlaying = true;
        recordingMsgPanel.SetActive(true);
//        SetBtnIntraction(false);
        Alphabet_PlayRecordDeleteHandler.instance.RecordingHandler();
        Alphabet_SoundManager.instance.StopIdleSound();
    }

    public void OpenLanguagePanel() {
        languageSelectionPanel.SetActive(true);
        //arCanvas.SetActive(false);
        spellCharacterParent.SetActive(false);
        screenShotTimer.SetActive(false);
        popUpActivityComplete.SetActive(false);
    }

    public void CloselanguagePanel() {
        languageSelectionPanel.SetActive(false);
        //arCanvas.SetActive(true);
        spellCharacterParent.SetActive(true);
        screenShotTimer.SetActive(true);
    }

    void CheckRecordSoundAvailablity()
    {
        if (Alphabet_PlayRecordDeleteHandler.instance.CheckRecordingAvalable())
        {
            deleteBtn.interactable = true;
            recordBtn.interactable = false;
        }
        else
        {
            deleteBtn.interactable = false;
            recordBtn.interactable = true;
        }
    }

    void HandleOnRecordingSaved() {
        CheckRecordSoundAvailablity();
        if (popUpActivityComplete != null) {
            popUpActivityComplete.SetActive(true);
            popUpActivityComplete.GetComponent<Animator>().Play("");
            popUpActivityComplete.GetComponentInChildren<Text>().text = "Recording Saved!";
        }
        Alphabet_SoundManager.instance.isSoundPlaying = false;
        Alphabet_SoundManager.instance.PlayIdleSound();
    }

    void HandleOnRecordingDelete()
    {
        CheckRecordSoundAvailablity();
        if (popUpActivityComplete != null)
        {
            popUpActivityComplete.SetActive(true);
            popUpActivityComplete.GetComponent<Animator>().Play("");
            popUpActivityComplete.GetComponentInChildren<Text>().text = "Recording Deleted!";
        }
    }

	public void CloseArBtns(){
		alphabetBtn.SetActive (false);
		centerMenu.SetActive (false);
	}


}
