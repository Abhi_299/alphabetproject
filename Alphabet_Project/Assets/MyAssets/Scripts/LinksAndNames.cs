﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinksAndNames
{

	public static string MenuJsonHeader = "Content";

	public static string pathToMenuJsonLocal = "/_Reusable/Json/";

	public static string nameOfMenuJsonFile = "json_response";

	public static string pathToMenujsonServer = "http://wizar.tech/testassets/json_response_China.json";

	public static string pathToMenuIconsServer = "http://wizar.tech/testassets/icons";

	public static string pathToMenuIconLocal = "/_Icons";

	public static string pathToFirebaseAuthentication = "https://wizarkids.firebaseio.com/";

	public static string pathToLocalActivatedKeys = "/Resources/_Reusable/Json/";

	public static string nameOFActivationKeyJsonFile = "json_ActivatedKeys";

	public static string activationKeyJsonHeader = "AuthKeys";

	public static string twitterURL = "https://twitter.com/wizartech";

	public static string facebookURL = "https://www.facebook.com/Wizar-Technologies-1844746999181181/";

	public static string youtubeURL = "https://www.youtube.com/user/ClassteacherCTLS/videos";

	public static string error01 = "Activation Not Allowed. Key has been used on maximum devices.";

	public static string error02 = "Could not connect to server, please check your internet connection.";

	public static string error03 = "Server is under maintenance, please be paitent and try after some time.";

	public static string error04 = "Wrong Activation Key. please check and re-enter the key.";

	public static string error05 = "This Activation key is not for the book you are tring to activate. Thankyou.";

	#if UNITY_ANDROID || UNITY_STANDALONE
	public static string PathToImages = "/mnt/sdcard/DCIM/WizAR/";
	public static string PathToThumbnail = "/mnt/sdcard/DCIM/WizAR/thumbnails/";
	#endif

	#if UNITY_IOS
	public static string PathToImages = Application.persistentDataPath + "/WizAR/";
	public static string PathToThumbnail = Application.persistentDataPath + "/WizAR/thumbnails/";
	#endif

}
