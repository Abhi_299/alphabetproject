﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class F_Alpha_G2_UIManager : MonoBehaviour {

    public static F_Alpha_G2_UIManager instance;

    public Animator basketAnim;
    public GameObject startPanel, gameOverPanel;
    public Text timerText;
    public Image[] iconPanel, gameOverIcon;
    public GameObject[] lifeLine;
    public Text[] iconScoreTxt, gameOverScoreTxt;

    [Header("Audio File")]
    public AudioSource entry_SoundPlayer;
    public AudioSource right_SoundPlayer, clock_SoundPlayer;

    //[Header("Particle Effect")]
    //public GameObject rightEffect;
    //public GameObject wrongEffect;

    private int[] scoreCount;
    private int lifeCount;
    internal Coroutine _particleRoutine;

    // Use this for initialization
    void Awake () {
        instance = this;
        scoreCount = new int[2];
        lifeCount = 3;
        startPanel.SetActive(true);
    }

    public void SetTimerValue(float value) {
        timerText.text = ((int)value).ToString();
        if (value < 6 && !clock_SoundPlayer.isPlaying) {
            clock_SoundPlayer.Play();
        }
    }

    public void IncreaseScore(int index) {
        scoreCount[index]++;
        iconScoreTxt[index].text = scoreCount[index].ToString();
        gameOverScoreTxt[index].text = scoreCount[index].ToString();
    }

    public void DecreaseLife() {

        Handheld.Vibrate();

        lifeCount--;
        lifeLine[lifeCount].SetActive(false);

        if (lifeCount == 0) {
            GameOver();
        }
    }

    public void GameOver() {
        f_Alpha_G2_GameManager.instance.FinishGame();
        gameOverPanel.SetActive(true);
        entry_SoundPlayer.Stop();
        right_SoundPlayer.Stop();
        clock_SoundPlayer.Stop();

    }

    //public IEnumerator SetRightEffect(bool status) {
    //    if (status)
    //    {
    //        Set_ParticleEffect(rightEffect);
    //    }
    //    else
    //    {
    //        Set_ParticleEffect(wrongEffect);
    //    }

    //    yield return new WaitForSeconds(0.8f);

    //    rightEffect.SetActive(false);
    //    wrongEffect.SetActive(false);
    //}

    //void Set_ParticleEffect(GameObject _particle) {
    //    rightEffect.SetActive(false);
    //    wrongEffect.SetActive(false);
    //    _particle.SetActive(true);
    //}

    public void SetBasketAnim(bool status) {
        if (status)
        {
            basketAnim.SetTrigger("blink");
        }
        else {
            basketAnim.SetTrigger("vibrate");
        }
    }

}
