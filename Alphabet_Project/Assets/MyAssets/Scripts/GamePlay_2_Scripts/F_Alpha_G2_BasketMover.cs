﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class F_Alpha_G2_BasketMover : MonoBehaviour {

    public Slider basketSlider;
    public Transform basket;

    public Vector3 basket_StartPoint, basket_EndPoint;

	// Use this for initialization
	void Start () {
        
	}

    public void OnSlider_Move() {
        basket.localPosition = Vector3.Lerp(basket_StartPoint, basket_EndPoint, basketSlider.value);
    }
	
}
