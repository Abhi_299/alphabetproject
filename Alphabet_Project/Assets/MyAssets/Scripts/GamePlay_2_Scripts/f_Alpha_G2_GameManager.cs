﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class f_Alpha_G2_GameManager : MonoBehaviour {

    public static f_Alpha_G2_GameManager instance;

    public Activity_ScriptableObject currObject_Data;
    public Transform _Camera;
    public float gapTime;
    public float activityTime;
    public Alphabet_Data_ScriptableObject alphabetData;
    //public ObjectData_G2[] alphabtObject;

    private List<GameObject> instantiated_Object;
    int currIndex, prevIndex = -1;
    Coroutine _ModelGenerator;
    bool startGame;

	// Use this for initialization
	void Start () {

        instantiated_Object = new List<GameObject>();

        if (activityTime == 0) {
            activityTime = 20;
        }

        if (_Camera.Find("BackgroundPlane") != null)
        {
            _Camera.Find("BackgroundPlane").gameObject.SetActive(false);
        }


        //Get selected alphabet index
        for (int i = 0; i < alphabetData.alphabetInfo.Count; i++)
        {
            if (alphabetData.alphabetInfo[i]._character.ToLower() == currObject_Data.current_Charater.ToLower()) {
                currIndex = i;
                Debug.Log("Curr index- " + i);
                break;
            }
        }

        //Instantiate alphabet models
        for (int i = 0; i < alphabetData.alphabetInfo[currIndex].alphabetModel.Count; i++)
        {
            GameObject obj = Instantiate(alphabetData.alphabetInfo[currIndex].alphabetModel[i].rightModel, Vector3.one * -2, Quaternion.Euler(Vector3.zero));
            obj.GetComponent<F_Alpha_ActivityTransformHandler>().scoreIndex = i;
            instantiated_Object.Add(obj);
            instantiated_Object.Add( Instantiate(alphabetData.alphabetInfo[currIndex].alphabetModel[i].wrongModel, Vector3.one * -2, Quaternion.Euler(Vector3.zero) ) );
            F_Alpha_G2_UIManager.instance.iconPanel[i].sprite = alphabetData.alphabetInfo[currIndex].alphabetModel[i].icon;
            F_Alpha_G2_UIManager.instance.gameOverIcon[i].sprite = alphabetData.alphabetInfo[currIndex].alphabetModel[i].icon;
        }

    }

    private void OnEnable()
    {
        instance = this;
        Invoke("SetCamera", 0.5f);
    }

    private void Update()
    {
        if (activityTime > 0 && startGame)
        {
            activityTime -= Time.deltaTime;
            F_Alpha_G2_UIManager.instance.SetTimerValue(activityTime);
        }
        else {
            F_Alpha_G2_UIManager.instance.clock_SoundPlayer.Stop();
        }
    }

    public void ClickOk_StartPanel() {
        F_Alpha_G2_UIManager.instance.startPanel.SetActive(false);
        _ModelGenerator = StartCoroutine(GenerateModel());
        startGame = true;
    }

    void SetCamera()
    {
        _Camera.localEulerAngles = Vector3.zero;
    }

    IEnumerator GenerateModel() {
        currIndex = 0;

        while (activityTime > 0)
        {
            while (currIndex == prevIndex)
            {
                currIndex = Random.Range(0, instantiated_Object.Count);
            }

            prevIndex = currIndex;

            F_Alpha_G2_UIManager.instance.entry_SoundPlayer.Play();
            yield return new WaitForSeconds(0.2f);
            F_Alpha_G2_MovingObjManager.instance.Set_MovingTargets(instantiated_Object[currIndex].transform);

            if (activityTime <= 0) {
                F_Alpha_G2_UIManager.instance.GameOver();
                break;
            }

            yield return new WaitForSeconds(gapTime);
        }
        F_Alpha_G2_UIManager.instance.GameOver();
    }

    public void FinishGame() {
        StopCoroutine(_ModelGenerator);

        for (int i = 0; i < F_Alpha_G2_MovingObjManager.instance.movingTargets.Length; i++)
        {
            F_Alpha_G2_MovingObjManager.instance.movingTargets[i].gameObject.SetActive(false);
        }

        activityTime = 0;
        F_Alpha_G2_UIManager.instance.SetTimerValue(activityTime);
    }
}

[System.Serializable]
public class ObjectData_G2
{
    public string _character;
    public List<Alphabet_Model_G2> alphabetModel;
}

[System.Serializable]
public class Alphabet_Model_G2
{
    public GameObject rightModel;
    public GameObject wrongModel;
    public Sprite icon;
}

