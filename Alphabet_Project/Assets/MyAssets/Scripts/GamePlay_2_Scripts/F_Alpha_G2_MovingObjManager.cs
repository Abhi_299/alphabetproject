﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F_Alpha_G2_MovingObjManager : MonoBehaviour {

    public static F_Alpha_G2_MovingObjManager instance;

    public float startPosition_X, endPosition_X;
    public float moveSpeed;

    public Transform[] movingTargets;

    internal int preIndex1 = -1, preIndex2 = -1, currIndex = 0;

	// Use this for initialization
	void Start () {
        instance = this;
        for (int i = 0; i < movingTargets.Length; i++)
        {
            movingTargets[i].gameObject.SetActive(false);
        }
	}

    public void Set_MovingTargets(Transform _transform) {

        currIndex = Random.Range(0, movingTargets.Length - 1);

        while (currIndex == preIndex1 || currIndex == preIndex2) {
            currIndex = Random.Range(0, movingTargets.Length);
        }


        preIndex2 = preIndex1;
        preIndex1 = currIndex;

        movingTargets[currIndex].localPosition = new Vector3(startPosition_X, movingTargets[currIndex].localPosition.y, movingTargets[currIndex].localPosition.z);
        movingTargets[currIndex].gameObject.SetActive(true);
        _transform.SetParent(movingTargets[currIndex]);

        _transform.localPosition = Vector3.zero;
        _transform.localEulerAngles = _transform.GetComponent<F_Alpha_ActivityTransformHandler>().rotation_G2;
        _transform.localScale = Vector3.one * _transform.GetComponent<F_Alpha_ActivityTransformHandler>().size_G2;
        _transform.gameObject.SetActive(true);

        if (_transform.GetComponentInChildren<Animator>() != null) {
            _transform.GetComponentInChildren<Animator>().SetBool("move", true);
        }

    }
}
