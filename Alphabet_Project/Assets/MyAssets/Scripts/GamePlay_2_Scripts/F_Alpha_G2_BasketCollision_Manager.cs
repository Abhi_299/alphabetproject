﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F_Alpha_G2_BasketCollision_Manager : MonoBehaviour {

    private string currLetter;

	// Use this for initialization
	void Start () {
        currLetter = f_Alpha_G2_GameManager.instance.currObject_Data.current_Charater.ToLower();
    }

    private void OnTriggerEnter(Collider col)
    {
        OnBasketHitting(col.transform);
    }

    public void OnBasketHitting(Transform _transform) {
        if (currLetter == _transform.GetComponent<Alphabet_ModelController>().modelInfoObject.alphabetCharacter.ToLower())
        {
            StartCoroutine(CorrectHit(_transform));
        }
        else
        {
            StartCoroutine(WrongHit(_transform));
        }

        _transform.gameObject.SetActive(false);
    }

    IEnumerator CorrectHit(Transform _transform) {
        F_Alpha_G2_UIManager.instance.right_SoundPlayer.Stop();
        F_Alpha_G2_UIManager.instance.right_SoundPlayer.Play();

        //if (F_Alpha_G2_UIManager.instance._particleRoutine != null)
        //{
        //    StopCoroutine(F_Alpha_G2_UIManager.instance._particleRoutine);
        //}
        //F_Alpha_G2_UIManager.instance._particleRoutine = StartCoroutine(F_Alpha_G2_UIManager.instance.SetRightEffect(true));

        F_Alpha_G2_UIManager.instance.SetBasketAnim(true);

        yield return new WaitForSeconds(0.2f);
        F_Alpha_G2_UIManager.instance.IncreaseScore(_transform.GetComponent<F_Alpha_ActivityTransformHandler>().scoreIndex);
    }

    IEnumerator WrongHit(Transform _transform)
    {
        //if (F_Alpha_G2_UIManager.instance._particleRoutine != null)
        //{
        //    StopCoroutine(F_Alpha_G2_UIManager.instance._particleRoutine);
        //}
        //F_Alpha_G2_UIManager.instance._particleRoutine = StartCoroutine(F_Alpha_G2_UIManager.instance.SetRightEffect(false));

        F_Alpha_G2_UIManager.instance.SetBasketAnim(false);

        yield return new WaitForSeconds(0.2f);
        F_Alpha_G2_UIManager.instance.DecreaseLife();
    }

}
