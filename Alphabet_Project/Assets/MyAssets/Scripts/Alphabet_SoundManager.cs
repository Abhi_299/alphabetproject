﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alphabet_SoundManager : MonoBehaviour {

    public static Alphabet_SoundManager instance;
    public AudioSource soundPlayer,idleSoundPlayer;
    public AudioClip timerSound, clickSound;

    internal bool isSoundPlaying;

    private AudioClip currentAlphabet_AudioClip;

    // Use this for initialization
    void Awake () {
        instance = this;
	}

    public void PlayTimerSound() {
        soundPlayer.PlayOneShot(timerSound);
    }

    public void PlayClickSound() {
        soundPlayer.PlayOneShot(clickSound);
    }

    public void Play_AudioClip(AudioClip _audioClip) {
        soundPlayer.PlayOneShot(_audioClip);
    }

    public void SetAlphabet_AudioClip(AudioClip _audioClip) {
            currentAlphabet_AudioClip = _audioClip;
    }

    public void PlayAlphabetSound() {
        if (currentAlphabet_AudioClip != null)
        {
            if (!isSoundPlaying) {
                soundPlayer.PlayOneShot(currentAlphabet_AudioClip);
                isSoundPlaying = true;
                Invoke("AfterPlayingSound", currentAlphabet_AudioClip.length+0.1f);
            }
            StopIdleSound();
        }
    }

    public void AfterPlayingSound() {
        isSoundPlaying = false;
        PlayIdleSound();
    }

    public void SetIdleSound(AudioClip _Clip) {
        if (idleSoundPlayer != null)
        {
            idleSoundPlayer.clip = _Clip;
            idleSoundPlayer.loop = true;
			if (!LoadAlphabet_OnDetection.instance._3dMode) {
				PlayIdleSound();
			}
        }
    }

    public void PlayIdleSound() {
        if (idleSoundPlayer!=null && idleSoundPlayer.clip != null)
        {
            idleSoundPlayer.Play();
        }
    }

    public void StopIdleSound() {
        if(idleSoundPlayer!=null)
        idleSoundPlayer.Stop();

    }
}
